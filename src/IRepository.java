/*
 * This Interface has been used for abstraction of methods used inside the Data Access object section of the Student
 * Registration System developed within the MVC(Model View Controller) model.
 * Methods defined within the interface are public and static by default and will be implemented within the child or implementing
 * class primarily for storage or interaction with data.
 * Methods:
 * Display(): To display contents of the primary data structure(.e.g. ArrayList) implemented within the program.
 * Add(Student person): To accept and add objects of Student class into the main data structure.
 * Delete(int id): To accept id of students and remove objects associated with the particular student from the main data structure.
 * Update(int id,String Name): To accept id and name of students and update the names of the particular student within the main data structure.
 *
@param
 */
public interface IRepository {
    void Display();
    void Add(Student person);
    void Delete(int id);
    void Update_Name(int id,String Name);

}
