/*
Class for initialization and creation of student credentials and Student class objects.
 */
public class Student {

    int id;
    String Name;
    String Phone;
    String Address;
    String Group;
   /*
   Constructor of initialization of student credentials.
    */
    public Student(int id,String Name,String Phone,String Address){
        this.id = id;
        this.Name=Name;
        this.Phone=Phone;
        this.Address=Address;
        this.Group= "";
    }

}

