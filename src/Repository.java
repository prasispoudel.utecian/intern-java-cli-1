import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/*
Class for implementing storage and data retrieval functionality within the application.
 */
public class Repository implements IRepository {

    ArrayList<Student> arrayList = new ArrayList<>();
    ArrayList<List> extra = new ArrayList<>();
    ArrayList<List> groupList = new ArrayList<>();
    HashMap<String,ArrayList> group = new HashMap<>();
    public void Add(Student person){
        arrayList.add(person);
        System.out.println("Added");
    }
    /*
    Method for retrieval and display of all Student Credentials.
     */
    public void Display(){
        for(Student obj: arrayList){
            System.out.println("ID:-"+ obj.id);
            System.out.println("Name:-"+ obj.Name);
            System.out.println("Phone:-"+obj.Phone);
            System.out.println("Address:-"+obj.Address);
        }
    }

/*
  Method for deletion of the student records indexed through the provided student id.
 */
    public void Delete(int id){
        int index =0 ;
        for(Student obj1: arrayList){
            if(obj1.id == id){
                index = arrayList.indexOf(obj1);
            }
        }
        arrayList.remove(index);
        System.out.println("Deleted");
    }


/*
Method for indexing and updating name of a student within the Student records.
 */

    public void Update_Name(int id,String name){
        for (Student obj2: arrayList){
            if(obj2.id == id){
                obj2.Name = name;
            }
        }
    }
}


