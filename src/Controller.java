/*
 Controller Class contains Main method for initiation of the Application and it's Command Line interface.
 */
public class Controller {
    public static void main(String[] args){
        Service serobj = new Service(); // Object of Service Class
        serobj.Main_menu();// Main_menu() method invocation for initiation of CLI of the Student Registration Application.

    }
}
