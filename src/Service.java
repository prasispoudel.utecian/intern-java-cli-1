import java.util.*;
/*
  Class for implementing all the core application logic  of the Student Registration System developed with MVC(Model View Controller) platform.
 */
public class Service implements IService {
    Repository repository = new Repository(); // Creation of Repository class object.
    /* Method for receiving  student credentials like id, Name, Phone Number , Address and initiation of storage of such credentials.
    @param int id,String Name, String Phone,String Address
     */
    public void Add(int id, String Name, String Phone, String Address) {
        repository.Add(new Student(id, Name, Phone, Address));
    }
    /* Method for initiating deletion of students records from the Main data structure.
     @param int id
     */
    public void delete(int id) {
        repository.Delete(id);
        repository.Display();
    }
    /*
    Method for initiating display of all the student records within the system.
     */
    public void Display() {
        System.out.println("--------------------------------");
        repository.Display();
        System.out.println("--------------------------------");
    }
    /*
     Method for dividing students into different groups.
     */
    public void generateGroup() {
        Scanner scc = new Scanner(System.in);
        System.out.println("Enter the number of students per group:- ");
        int num = scc.nextInt();
        int size = repository.arrayList.size();
        //System.out.println(repository.arrayList.size());
        double num2 = repository.arrayList.size() / num;
        int num3 = repository.arrayList.size()%num;
        for(List arrr:repository.extra){
            System.out.println(arrr);
        }
        for(int i =0;i<=((repository.arrayList.size()%num)-1);i++){
            repository.arrayList.remove(i);
        }
        String str = Double.toString(num2);
        String[] arr = str.split("\\.");
        int a = Integer.parseInt(arr[0]);
        //int b = Integer.parseInt(arr[1]);
        int b= repository.arrayList.size()%num;
        if((num3)>=3){
            int val = (size-num3)+(size-num3)/a;
            repository.group.put("G"+String.valueOf(val),this.Divide(0,num3));
        }
        this.RangeSet(a,b);
        this.getGroup();
        if((num3)<=2){
            //int val1 = (size-num3)+(size-num3)/a;
            for(String ob: repository.group.keySet()){
               // Code Remainning
            }

        }
        this.getGroup();
    }
   /*
   Method for division of records from the main arraylist into different segments for storage in key value pairs in a Hash Map dataStructure.
    */
      public void RangeSet(int x, int y){
          int reg = (repository.arrayList.size() - y) / x;
          int srt =0;
          int num3 = 0;
          do {
              for(int i =0; i<x;i++){
                  repository.group.put("G"+String.valueOf(reg), this.Divide(srt, reg));
                  srt += ((repository.arrayList.size() - y) / x);
                  reg += ((repository.arrayList.size() - y) / x);
              }
          }while (num3<=x && repository.arrayList.size()>=reg);
    }

    /*
    Method for segmenting an given ArrayList into smaller segments according to specified requirements.
     */
       public ArrayList Divide(int srt,int reg) {
           ArrayList<Student> ar = new ArrayList(repository.arrayList.subList(srt, reg));
           return ar;
        }

        /*
        Method for displaying contains of key value pair within a HashMap data structure implemented within the application.
         */
  public void getGroup(){
       for(String ob: repository.group.keySet()){
           System.out.println("Keys:- "+ob);
           System.out.println("Values:- "+ repository.group.get(ob));
       }
    }
  /*
  Method for initiation of procedures for updating students name within the records.
   */
    public void UpdateName(int id, String Name) {
        repository.Update_Name(id, Name);
    }
/*
Method implementing the CLI(Command Line Interface) of the Student Registration Application.
 */
    public void Main_menu(){
        System.out.println("---------------MENU------------------\n Choose one of the Following Options.\n 1. Register a New Student \n 2. Remove Existing Student. \n 3. Update Student Name  \n 4. Display the Details of the Updated Students\n5. Generate Groups\n 6. Shutdown ");
        System.out.println("Enter your Value:- ");
        Scanner obj = new Scanner(System.in);
        int choice = obj.nextInt();
        System.out.println("Your have selected option:- " + choice);
        if(choice == 1){
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the id,Name,Phone,Address of the Student you want to register");
            int input_id = sc.nextInt();
            System.out.println("Enter name:- ");
            Scanner sc1 = new Scanner(System.in);
            String input_name = sc1.nextLine();
            System.out.println("--------------------`----------");
            Scanner sc2 = new Scanner(System.in);
            System.out.println("Enter the phone number:- ");
            String input_phone = sc2.nextLine();
            System.out.println("------------------------------");
            Scanner sc3 = new Scanner(System.in);
            System.out.println("Enter the Address:- ");
            String input_address = sc3.nextLine();
            System.out.println("---------------------------");
            this.Add(input_id,input_name,input_phone,input_address);
            this.Main_menu();
        } else if(choice ==2) {
            this.Display();
            System.out.println("Enter the id of the student for deletion:-  ");
            int del_int = obj.nextInt();
            this.delete(del_int);
            this.Main_menu();
        }else if(choice ==3){
            this.Display();
            System.out.println("Enter the following credentials for updating student name in records");
            System.out.println("Id:- ");
            int up_id = obj.nextInt();
            System.out.println("Name:- ");
            Scanner sc5 = new Scanner(System.in);
            String up_name = sc5.nextLine();
            this.UpdateName(up_id,up_name);
            this.Main_menu();
        }else if(choice ==4){
            System.out.println("Displaying Records ......");
            this.Display();
            this.Main_menu();
        }else if(choice ==5){
            if(repository.arrayList.size() != 0) {
                this.generateGroup();
            }else{
                System.out.println("No Students");
            }
            this.Main_menu();
        }else if(choice == 6){
            System.out.println("Shutting Down System");
            System.exit(0);
        }
        else {
            System.out.println("Incorrect Option Selected");
        }

    }
}



