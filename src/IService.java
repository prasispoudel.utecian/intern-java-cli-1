/*
 * This Interface has been created for abstraction of methods used inside the Service section(Business logic/application section) of the Student
 * Registration System developed within the MVC(Model View Controller) model.
 * Methods defined within the interface are public and static by default and will be implemented within the child or implementing
 * class for implementing the core Application logic.
 * Methods:
 * Display(): To initiate the display contents of the primary data structure(.e.g. ArrayList) implemented within the Repository class the program.
 * Add(int id,String Name,String Phone,String Address): To accept student credentials and initialize unique Student class objects for storage within
 * the main data structure.
 * delete(int id): To accept id of students and initialize procedure of removal of objects associated with the particular student from the main data structure.
 * UpdateName(int id,String Name): To accept id and name of students and initialize procedures for updating the name of the particular student within the main data structure.
 *
@param
 */


public interface IService {
    void Add(int id, String Name, String Phone, String Address);
    void delete(int id);
    void Display();
    void UpdateName(int id, String Name);
    void Main_menu();
}
